**Node JS MQTT Project**

Le projet suit les consignes suivantes :
- Building a Node.js based MQTT server
- Think to how to build your MQTT server based on Node.js.
- Provides which package you have to install and use.
- Install this package and provide a very short example of and MQTT server saying “Hello IOT World !_

Le projet utilise la librairie 'aedes' pour créer le broker et 'mqtt' pour le subscriber et le publisher.

**Pour lancer le projet, utiliser les commandes suivantes dans 3 terminaux différents :**

- node broker
- node subscriber
- node publisher

Le publisher envoie le message "Hello IOT world !" au broker. Le broket retransmet le message envoyé par le publisher au subscriber.


